
public class DateTime {
    private int sec;
    private int min;
    private int hour;
    private int day;

    public DateTime(int day, int hour, int minute, int second) {
        this.sec = second;
        this.min = minute;
        this.hour = hour;
        this.day = day;
    }

    public void addDate(DateTime dateTime) {
        int i;
        this.sec = (this.sec + dateTime.sec) % 60;
        i = (this.sec + dateTime.sec) / 60;
        this.min = (this.min + dateTime.min + i) % 60;
        i = (this.min + dateTime.min) / 60;
        this.hour = (this.hour + dateTime.hour + i) % 24;
        i = (this.hour + dateTime.hour) / 24;
        this.day = (this.day + dateTime.day + i);
    }

    public boolean isLaterThan(DateTime dateTime) {
        if (this.day > dateTime.day)
            return true;
        else if (this.day == dateTime.day && this.hour > dateTime.hour)
            return true;
        else if (this.day == dateTime.day && this.hour == dateTime.hour && this.min > dateTime.min)
            return true;
        else if (this.day == dateTime.day && this.hour == dateTime.hour && this.min > dateTime.min && this.sec > dateTime.sec)
            return true;
        return false;
    }

    public boolean isEarlierThan(DateTime dateTime) {
        if (this.day < dateTime.day)
            return true;
        else if (this.day == dateTime.day && this.hour < dateTime.hour)
            return true;
        else if (this.day == dateTime.day && this.hour == dateTime.hour && this.min < dateTime.min)
            return true;
        else if (this.day == dateTime.day && this.hour == dateTime.hour && this.min < dateTime.min && this.sec < dateTime.sec)
            return true;
        return false;
    }

    @Override
    public String toString() {
        return String.valueOf(day) + " days " + String.valueOf(hour) + " hours " + String.valueOf(min) + " minutes " + String.valueOf(sec) + " seconds";
    }
}
