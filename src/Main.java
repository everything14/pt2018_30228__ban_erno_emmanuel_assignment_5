import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class Main {
    private List<MonitoredData> monitoredData;
    private static final String PATH = "Activities.txt";

    private void readFile() {
        List<String> data = Collections.emptyList();
        try {
            data = Files.readAllLines(Paths.get(PATH), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<MonitoredData> monitoredData = new ArrayList<>();
        data.forEach(l -> {
            List<String> line =  Arrays.stream(l.split("(\\t)+")).collect(Collectors.toList());
            String startTime = line.get(0);
            String endTime = line.get(1);
            String activity = line.get(2);
            monitoredData.add(new MonitoredData(startTime, endTime, activity));
        });
        this.monitoredData = monitoredData;
    }

    private void printDistinctActivitiesCount() {
        List<String> activities = new ArrayList<>();
        getDays().stream().distinct().forEach(d ->
                activities.addAll(getActivities(d))
        );
        Map<String, Integer> activitiesCount;
        activitiesCount = activities.stream().collect(groupingBy(Function.identity(), summingInt(e -> 1)));
        PrintWriter writer;
        try {
            writer = new PrintWriter("DistinctActivitiesCount", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        for (Map.Entry<String, Integer> entry : activitiesCount.entrySet()) {
            writer.println(entry.getKey() + " " + entry.getValue());
        }
        writer.close();
    }

    private void printDailyResults() {
        PrintWriter writer;
        try {
            writer = new PrintWriter("DailyResults", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        Map<String, Map<String, Integer>> dailyMap = new HashMap<>();
        for (String day : getDays().stream().distinct().collect(Collectors.toList())) {
            dailyMap.put(day, getDailyActivitiesCount(day));
            writer.println();
            writer.println(day);
            for (Map.Entry<String, Integer> entry : getDailyActivitiesCount(day).entrySet()) {
                writer.println(entry.getKey() + " " + entry.getValue());
            }
        }
        writer.close();
    }

    private void printLargeActivities() {
        PrintWriter writer;
        try {
            writer = new PrintWriter("LargerActivities", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        Map<String, DateTime> timeMap = new HashMap<>();
        for(Map.Entry<String, List<DateTime>> e : getActivityList().entrySet()) {
            DateTime dateTime = new DateTime(0,0,0,0);
            e.getValue().forEach(date -> dateTime.addDate(date));
            timeMap.put(e.getKey(), dateTime);
        }
        Map<String, DateTime> largeActivities = timeMap.entrySet().stream().filter(
                t -> t.getValue().isLaterThan(new DateTime(0, 10, 0, 0))
        ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        for (Map.Entry<String, DateTime> e : largeActivities.entrySet()) {
            writer.println(e.getKey() + "\t\t" + e.getValue());
        }
        writer.close();
    }

    public void printShortActivities() {
        PrintWriter writer;
        try {
            writer = new PrintWriter("ShortActivities", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        List<String> smallActivities = new ArrayList<>();
        getActivityList().forEach((key, value) -> {
            List<DateTime> dateTimeList = value.stream().filter(t -> t.isEarlierThan(new DateTime(0, 0, 5, 0))
            ).collect(Collectors.toList());
            if (10*dateTimeList.size() > 9*value.size()) {
                smallActivities.add(key);
            }
        });
        smallActivities.forEach(s -> writer.println(s));
        writer.close();
    }


    public static void main(String[] args) {
        Main main = new Main();
        main.readFile();
        System.out.println("There are " + main.getTotalOfDays() + " days of monitored data in the log");
        main.printDistinctActivitiesCount();
        main.printDailyResults();
        main.printLargeActivities();
        main.printShortActivities();
    }

    private int getTotalOfDays() {
        return getDays().stream().distinct().collect(Collectors.toList()).size();
    }

    private Map<String, Integer> getDailyActivitiesCount(String day) {
        List<String> activities = new ArrayList<>();
        activities.addAll(getActivities(day));
        return activities.stream().collect(groupingBy(Function.identity(), summingInt(e -> 1)));
    }

    private List<String> getDays() {
        List<String> allDays = new ArrayList<>();
        for (MonitoredData m : monitoredData) {
            allDays.add(m.getStartTime().split((" "))[0]);
        }
        return allDays;
    }

    private List<String> getActivities(String day) {
        List<String> activities = new ArrayList<>();
        List<MonitoredData> dailyData = monitoredData.stream().filter(m -> day.equals(m.getStartTime().split((" "))[0])).collect(Collectors.toList());
        for (MonitoredData m : dailyData) {
            activities.add(m.getActivity());
        }
        return activities;
    }

    private DateTime getDateTime(MonitoredData monitoredData) {
        String[] endValues = monitoredData.getEndTime().split(" |-|:");
        LocalDateTime endTime = LocalDateTime.of(Integer.valueOf(endValues[0]), Integer.valueOf(endValues[1]), Integer.valueOf(endValues[2]), Integer.valueOf(endValues[3]), Integer.valueOf(endValues[4]), Integer.valueOf(endValues[5]));
        String[] startValues = monitoredData.getStartTime().split(" |-|:");
        LocalDateTime startTime = LocalDateTime.of(Integer.valueOf(startValues[0]), Integer.valueOf(startValues[1]), Integer.valueOf(startValues[2]), Integer.valueOf(startValues[3]), Integer.valueOf(startValues[4]), Integer.valueOf(startValues[5]));
        long t = startTime.until(endTime, ChronoUnit.SECONDS);
        return new DateTime((int)(t/60/60/24), (int)(t/60/60)%24, (int)(t/60)%60, (int)(t%60));
    }

    private Map<String, List<DateTime>> getActivityList() {
        Map<String, List<DateTime>> activityMap = new HashMap<>();
        monitoredData.forEach(m -> {
            List<DateTime> dateTimes;
            if (!(activityMap.containsKey(m.getActivity()) && activityMap.get(m.getActivity()) != null)) {
                dateTimes = new ArrayList<>();
            }
            else {
                dateTimes = activityMap.get(m.getActivity());
            }
            dateTimes.add(getDateTime(m));
            activityMap.put(m.getActivity(), dateTimes);
        });
        return activityMap;
    }
}
